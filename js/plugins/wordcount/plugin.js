(function () {



    CKEDITOR.plugins.add('wordcount', {
        requires: [],
        init: function (editor) {
            //function to grab the current wordcount
            var getWC = function () {
                //grab full html content
                var data = editor.getData();
                data = data
                    //Add your custom regex replaces here!

                    //remove all tags
                    .replace(/(<([^>]+)>)/ig, "")
                    //remove all newlines
                    .replace(/\n/ig, " ")
                    //remove any remaining random &nbsp;
                    .replace(/&nbsp;/ig, " ")
                    //trim all multi spaces down to single spaces
                    .replace(/\s\s+/g, ' ')
                    //or here, i believe in you.

                    //trim all the leading/trailing whitespace
                    .trim()
                    //split based on spaces
                    .split(" ");

                
                //if the editor is empty, return 0, else return the wordcount
                return data.length === 1 && data[0] === "" ? 0 : data.length;
            }

            editor.ui.addButton('wordcount', {
                label: Drupal.t('WordCount'),
                command: 'wordcount',
                icon: this.path + 'icons/wordcount.png'
            });

            editor.on('loaded', function () {
                //hide the button on the wysiwyg as it does nothing and may confuse the end user
                editor.ui.space('top').find('.cke_button__wordcount').$[0].style.display = "none";
                //grab the bottom space html
                var bottom = editor.ui.space('bottom');
                //append an initial wordcount element
                bottom.appendHtml('<span id="WordCount" style="float:right;">Word Count: ' + getWC() + '</span>');

            });

            editor.on('change', function () {
                //if something went wrong with initializing the wordcount, do nothing on change
                if (!editor.ui.space('bottom').find('#WordCount').$.length) {
                    return;
                }
                //find the wordcount and update the inner text!
                editor.ui.space('bottom').find('#WordCount').$[0].innerText = 'Word Count: ' + getWC();

            });


        },

        afterInit: function (editor) {

        }
    })
})()