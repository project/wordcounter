<?php

/**
 * @file
 * Definition of \Drupal\wordcount\Plugin\CKEditorPlugin\WordCount.
 */

namespace Drupal\wordcount\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "WordCount" plugin.
 *
 * @CKEditorPlugin(
 *   id = "wordcount",
 *   label = @Translation("WordCount Button")
 * )
 */
class WordCount extends CKEditorPluginBase {

   /**
    * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::isInternal().
    */
   public function isInternal() {
     return FALSE;
   }
   
   /**
    * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::getFile().
    */
   public function getFile() {
      return drupal_get_path('module', 'wordcount') . '/js/plugins/wordcount/plugin.js';
   }
   
   /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginButtonsInterface::getButtons().
   */
   public function getButtons() {
      return [
         'wordcount' => [
            'label' => t('WordCount'),
            'image' => drupal_get_path('module', 'wordcount') . '/js/plugins/wordcount/icons/wordcount.png'
         ]
      ];
   }
   
   /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginButtonsInterface::getConfig().
   */
   public function getConfig(Editor $editor) {
      return [];
   }

}