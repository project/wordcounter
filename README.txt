INTRODUCTION
------------

The WordCount module adds a simple word counter to the bottom section of the CKEditor ui.
One day I may add a config page for custom regex, until then, you can add custom replaces at
wordcount->js->plugins->wordcount->plugins.js
For help:
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/replace

REQUIREMENTS
------------

This module requires no modules outside of Drupal core, only CKEditor.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

Go to /admin/config/content/formats

Configure each text format you want WordCount on by dragging the --blue/white WC--
toolbar button to the active toolbar. You can place it anywhere as it will be 
hidden from the users. The button itself doesn't do anything and I haven't figured out 
how to make a plugin that doesn't use a button. Plus, you'll be able to tell if WordCount is working
if you see the word count in bottom right of the editor!

